<?php

namespace Validation;

/**
 * Правило обязательного заполнения поля
 */
class RequiredRule extends BaseRule
{
	protected $check_on_empty=true;
	public function isValid($data)
	{
		return !empty($data->{$this->field_name});
	}
}