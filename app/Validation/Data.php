<?php
namespace Validation;

/**
 * Объект безопасной работы с данными
 */
class Data
{
	protected $data_array;
	protected $fields_list;
	/**
	 * @param array $data_array  данные запроса
	 * @param array  $fields_list список полей для фильтра при выводе
	 */
	public function __construct($data_array,$fields_list=[])
	{
		$this->data_array=$data_array;
		$this->fields_list=$fields_list;
	}
	public function __get($f_name)
	{
		return isset($this->data_array[$f_name])
			?$this->data_array[$f_name]
			:null;
	}
	public function __set($f_name,$value)
	{
		$this->data_array[$f_name]=$value;
	}
	public function __isset($f_name)
	{
		return isset($this->data_array[$f_name]);
	}
	public function __unset($f_name)
	{
		$ind=array_search($f_name, $this->fields_list);
		if($ind!==false)
			unset($this->fields_list[$ind]);
		unset($this->data_array[$f_name]);
	}
	/**
	 * Фильтр выдает только те поля которые указаны во входном массиве
	 * @param  array $array_fields перечень полей для вывода
	 * @return array               массив отфильтрованных значений
	 */
	public function only($array_fields)
	{
		$arr=[];
		foreach($array_fields as $field_name)
			$arr[$field_name]=$this->$field_name;
		return $arr;
	}
	/**
	 * Генерация массива из данных
	 * @param  boolean $only_listed если true исключает те, которые не указаны в списке полей конструктора
	 * @return array               массив данных
	 */
	public function toArray($only_listed=false)
	{
		if($only_listed)
			return $this->only($this->fields_list);

		return array_merge(array_combine($this->fields_list,array_fill(0, count($this->fields_list), null)),$this->data_array);
	}
}