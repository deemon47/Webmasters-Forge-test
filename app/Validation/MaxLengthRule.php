<?php
namespace Validation;

/**
 * Проверка максимальной длинны
 */
class MaxLengthRule extends BaseRule
{

	public function isValid($data)
	{
		return mb_strlen($data->{$this->field_name}) <= $this->params[0];
	}
}