<?php

namespace Validation;

/**
 * Правило поля типа email
 */
class EmailRule extends BaseRule
{
	protected $pattern='/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
	public function isValid($data)
	{
		return preg_match($this->pattern, $data->{$this->field_name});
	}
}