<?php

namespace Controllers;
use View;
use Helpers;
use Auth;
use DB;
/**
 * Главный контроллер
 */
class MainController extends BaseController
{
	// Вывод главной страницы
	public function anyIndex()
	{
		$result=DB::query('SELECT full_name,avatar,created_at FROM users ORDER BY id DESC LIMIT 1 ');
		$last_user=$result->fetch();
		return new View('main',compact('last_user'));
	}
	// Вывод страницы о сайте
	public function getAbout()
	{
		return new View('about');
	}

	// Вывод страницы профиля пользователя
	public function getProfile()
	{
		if(!Auth::check())
			app('redirect',app('url','',false).'#auth');

		return new View('profile');
	}

	// Генерация файла локализации для пользовательской части  с использованием браузерного кеша и автоматической генерацией в случае обновления исходных файлов
	public function getLocale()
	{
		$json_path=Helpers::getPath('storage/'.app('getLocale').'_locale.json');
		$create=true;
		$json_at=time();
		if(file_exists($json_path))
		{
			$validation_at=filemtime(Helpers::getPath('resources/lang/'.app('getLocale').'/validation.php'));
			$fields_at=filemtime(Helpers::getPath('resources/lang/'.app('getLocale').'/fields.php'));

			$json_at=filemtime($json_path);
			if($json_at>$validation_at
				&& $json_at>$fields_at)
				$create=false;
		}
		if($create)
			file_put_contents($json_path, 'var locale='. json_encode([
				'validation'=>app('trans','validation'),
				'fields'=>app('trans','fields'),
			]));
		// Использование браузерного кеша
		$seconds_to_cache = 3600;
		$expries_at = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
		$json_at = gmdate('D, d M Y H:i:s', $json_at) . ' GMT';
		header('Expires: '.$expries_at);
		header('Last-Modified: '.$json_at);
		header('Pragma: cache');
		header('Cache-Control: max-age='.$seconds_to_cache);
		header('Content-Type:application/json');
		readfile($json_path);
		die();
	}
}