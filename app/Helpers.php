<?php
use Exceptions\BaseException;
/**
 * Helpers
 */
class Helpers
{
	private static $_array_file_value_cache=[];

	/**
	 * Генерация абсолютного пути от корня приложения
	 * @param  string $path относительный путь
	 * @return string       абсолютный путь
	 */
	public static function getPath($path)
	{
		return ROOT_PATH.$path;
	}
	/**
	 * Преобразование текста через символ в "верблюжье" написание
	 * @param  string $string    входная строка
	 * @param  string $separator разделительный символ
	 * @return string            строка в "верблюжьем" написании
	 */
	public static function dashToCamelCase($string,$separator='-')
	{
		$arr=explode($separator, $string);
		$arr=array_map('ucfirst', $arr);
		return implode('', $arr);
	}
	/**
	 * Исключение полей из массива
	 * @param  array $array массив данных
	 * @param  array $keys  массив индексов исключения
	 * @return array        отфильтрованный массив
	 */
	public static function array_except($array,$keys)
	{
		foreach($keys as $key)
			unset($array[$key]);
		return $array;
	}
	/**
	 * Возвращает занчение массива по массиву ключей
	 * @param  array $array   данные
	 * @param  array $keys    ключи
	 * @param  mixed $default занчение по умолчанию
	 * @return mixed          найденное занчение или занчение по умолчанию
	 */
	public static function getArrayValue($array,$keys,$default=null)
	{
		if(count($keys)==0)
			return $array;
		$key=array_shift($keys);
		if(isset($array[$key]))
		{
			$value=$array[$key];
			return static::getArrayValue($value,$keys,$default);
		}
		return $default;
	}
	/**
	 * Поиск значения в файле массива
	 * @param  string $base_dir   директория расположения файла
	 * @param  string $value_path строка условного адреса
	 * @param  mixed $default    занчение по умолчанию
	 * @return mixed             найденное занчение или занчение по умолчанию
	 */
	public static function getArrayFileValue($base_dir,$value_path,$default=null)
	{
		$value_path_arr=explode('.', $value_path);
		if(count($value_path_arr)<1)
			throw new BaseException('Wrong key path: '.$value_path );


		$file_path=$base_dir.array_shift($value_path_arr).'.php';

		$arr=null;
		if(isset(static::$_array_file_value_cache[$file_path]))
			$arr=static::$_array_file_value_cache[$file_path];
		else
		{
			if(!file_exists($file_path))
				throw new BaseException('Array file doesn\'t exists: '. $file_path);
			$arr=include_once($file_path);
			static::$_array_file_value_cache[$file_path]=$arr;
		}
		return static::getArrayValue($arr,$value_path_arr,$default);
	}
}