<?php
namespace Exceptions;


/**
 *  Исключение в случае не верного CSRF токена
 */
class TokenMismatchException  extends BaseException
{
	public function render()
	{
		if(app('isApiRequest'))
		{
			http_response_code(401);
			echo json_encode(
			[
				'redirect'=>'reload',
				'redirect_timeout'=>10000,
				'message'=>app('trans','main.token_mismatch'),
			]);
			die();
		}

		app()->redirect();
	}
}