<?php
namespace Exceptions;

/**
 * Исключения для вывода сообщения
 */
class MessageException extends BaseException
{
	/**
	 * @param string $message сообщение для показа
	 */
	public function __construct($message)
	{
		$this->message=$message;
	}
	public function render()
	{
		http_response_code(422);
		echo json_encode([
			'message'=>$this->message,
		]);
		die();
	}
}