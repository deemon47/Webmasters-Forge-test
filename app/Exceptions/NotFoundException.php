<?php
namespace Exceptions;

use View;

/**
 *  ИИсключение в случае не верного УРЛ
 */
class NotFoundException  extends BaseException
{
	public function render()
	{
		http_response_code(404);
		$view=new View('404');
		$view->render();
	}
}