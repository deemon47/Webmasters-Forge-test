<?php
return
[
	// Доступные языки
	'available_locales'=>
	[
		'ru',// язык с индексом 0 - язык по умолчанию
		'en',
	],
	'database'=>
	[
		'host'=>'localhost',
		'port'=>'3306',
		'user_name'=>'root',
		'password'=>'',
		'db_name'=>'wm-forge-test',
		'charset'=>'utf8',
	],
	// Время жизни токена авторизации
	'token_lifetime'=>24,
];