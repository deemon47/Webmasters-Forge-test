//'use strict';
var gulp=require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var cssmin = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var debug = require('gulp-debug');
var filter = require('gulp-filter');
var imagemin = require('gulp-imagemin');
var include = require('gulp-include');
var imageminOptipng = require('imagemin-optipng');
var spritesmith = require('gulp.spritesmith');

var root_path=__dirname;
var js_dest='public/js';
var css_dest='public/css';
var images_dest='public/images';
var copy_dest='public/';
var copy_task=
[
	{"src":"node_modules/font-awesome/fonts/*.*","dest":"/fonts"}
];
var minify=true;
var css_compatibility="ie9";
var include_config=
{
    includePaths:
    [
	   root_path+"/node_modules",
		root_path+"/vendor",
		root_path+"/resources/assets/js",
    ]
}

gulp.task('copy', function () {

	gulp.src([root_path+'/resources/assets/copy/**/*.*','!'+root_path+'/resources/assets/copy/index.php'])
		.pipe(debug({'title':'copy'}))
		.pipe(gulp.dest(copy_dest));

	if(copy_task)
		for(var key in copy_task)
		{
			var val=copy_task[key];
			gulp.src(val.src)
			.pipe(debug({'title':'copy_task'}))
			.pipe(gulp.dest(copy_dest+val.dest));

		}

	return true;
});

gulp.task('images', function () {
	// Use all normal and `-2x` (retina) images as `src`
	//	 e.g. `github.png`, `github-2x.png`
	var spriteData = gulp.src(root_path+'/resources/assets/images/_icons/*.png')
		.pipe(debug({title:"image"}))
		.pipe(spritesmith({
			// Filter out `-2x` (retina) images to separate spritesheet
			//	 e.g. `github-2x.png`, `twitter-2x.png`
			// retinaSrcFilter: '*-2x.png',

			// Generate a normal and a `-2x` (retina) spritesheet
			imgName: 'icons.png',
			// retinaImgName: 'spritesheet-2x.png',

			// Generate SCSS variables/mixins for both spritesheets
			cssName: 'sprites.scss',
			imgPath:'../images/icons.png'
		}));

	spriteData.img
		.pipe(gulp.dest(images_dest));

	spriteData.css.pipe(gulp.dest(root_path+'/storage/tmp/assets/'));

	gulp.src([
			root_path+'/resources/assets/images/**/*.*',
			'!'+root_path+'/resources/assets/images/_icons/*.*',
			'!*.png'
		])
		.pipe(imagemin())
		.pipe(gulp.dest(images_dest));

	imagemin([root_path+'/resources/assets/images/*.png',
		'!'+root_path+'/resources/assets/images/_icons/*.*'
		], images_dest, {use: [imageminOptipng()]});

	return true;
});

gulp.task('js', function () {
	var f=filter(['**','!**/*.min.js'],{restore:true});
	// ============= VENODR LIB JS===========
	var task =gulp
		.src(root_path+'/resources/assets/js/libs.js')
		//.pipe(sourcemaps.init())
		.pipe(include(include_config));
	if(minify)
		task=task
			.pipe(f)
			.pipe(debug({title:'filter:'}))
			.pipe(uglify({
				//output:{comments:'all'}
				}))
			.pipe(f.restore);
	task=task
		.pipe(debug({title: 'libs.js:'}))
		.pipe(concat('libs.min.js'))
		// .pipe(sourcemaps.write('.',{includeContent:false,sourceRoot:"src/js/lib"}))
		.pipe(gulp.dest(js_dest));


	var f=filter(['**','!**/*.min.js'],{restore:true});
	task=gulp
		.src(root_path+'/resources/assets/js/site.js')
		.pipe(sourcemaps.init())
		.pipe(include(include_config))
		.pipe(debug({title:'filter:'}))
	if(minify)
		task=task
			.pipe(f)
			.pipe(debug({title:'filter:'}))
			.pipe(uglify())
			.pipe(f.restore);
	task=task
		// .pipe(debug({title: 'libs.js:'}))
		.pipe(concat('site.min.js'))
		// .pipe(sourcemaps.write('.',{includeContent:false,sourceRoot:"src/js/site"}))
		.pipe(gulp.dest(js_dest));

	var f=filter(['**','!**/*.min.js'],{restore:true});
	task=gulp
		.src(root_path+'/resources/assets/js/admin.js')
		.pipe(sourcemaps.init())
		.pipe(include(include_config))
		.pipe(debug({title:'filter:'}))
	if(minify)
		task=task
			.pipe(f)
			.pipe(debug({title:'filter:'}))
			.pipe(uglify())
			.pipe(f.restore);
	task=task
		// .pipe(debug({title: 'libs.js:'}))
		.pipe(concat('admin.min.js'))
		// .pipe(sourcemaps.write('.',{includeContent:false,sourceRoot:"src/js/site"}))
		.pipe(gulp.dest(js_dest));

	return task;
});

// ==========CSS
gulp.task('css',function () {

	task =gulp.src(root_path+'/resources/assets/scss/site.scss')
		// .pipe(debug({title: 'scss:'}))
		.pipe(sourcemaps.init())
		// .pipe(include({hardFail :true}))
		.pipe(sass())
		// .on('error', console.log)
		.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false,
            remove: false,
        }))
		.pipe(concat('site.min.css'));

	if(minify)
		task=task
			.pipe(cssmin({compatibility: css_compatibility}))
	task=task
		// .pipe(sourcemaps.write('.',{includeContent:false,sourceRoot:"src/css"}))
		.pipe(gulp.dest(css_dest));

	task =gulp.src(root_path+'/resources/assets/scss/admin.scss')
		// .pipe(debug({title: 'scss:'}))
		.pipe(sourcemaps.init())
		// .pipe(include({hardFail :true}))
		.pipe(sass())
		// .on('error', console.log)
		.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false,
            remove: false,
        }))
		.pipe(concat('admin.min.css'));

	if(minify)
		task=task
			.pipe(cssmin({compatibility: css_compatibility}))
	task=task
		// .pipe(sourcemaps.write('.',{includeContent:false,sourceRoot:"src/css"}))
		.pipe(gulp.dest(css_dest));

	return task;
})


gulp.task('css_watch',function ()
{
	var css_watcher = gulp.watch([
		root_path+'/resources/assets/scss/**/*.scss'
		], ['css']);
	css_watcher.on('change', function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});
})
gulp.task('js_watch',function ()
{
	var js_watcher = gulp.watch([
		root_path+'/resources/assets/js/**/*.js'
		], ['js']);
	js_watcher.on('change', function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});
})
gulp.task('images_watch',function ()
{
	var js_watcher = gulp.watch([
		root_path+'/resources/assets/images/**/*.*',
		'!'+root_path+'/resources/assets/images/icons.png',
	], ['images','css']);
	js_watcher.on('change', function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});
})

gulp.task('build',['images','js','css','copy']);

gulp.task('default',['js','css','js_watch','css_watch','images_watch']);
