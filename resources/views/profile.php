<?php
$view->title='profile';
include_once('_key_value.php');
$view->content=function(){
$user=Auth::user();
	?>
<?php key_value(app('trans','fields.id',[],true),$user['id'])?>
<?php key_value(app('trans','fields.full_name',[],true),$user['full_name'])?>
<?php key_value(app('trans','fields.email',[],true),$user['email'])?>
<?php key_value(app('trans','fields.phone',[],true),$user['phone'])?>
<?php key_value(app('trans','fields.created_at',[],true),$user['created_at'])?>
<div class="row">
	<div class="text-right col-xs-6"><b><?php app('trans','fields.avatar')?></b></div>
	<div class="col-xs-6 text-left">
		<div class="thumbnail avatar" >
			<img src="<?php echo Auth::getUserAvatar() ?>">
		</div>
	</div>
</div>
<?php };
include 'layout.php';