<?php
$view->title='signup';
include_once('_t_field.php');
include_once('_alert.php');

$view->content=function()use ($view){?>
<form action="<?php app('url','auth/signup')?>" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="csrf_token" value="<?php app()->getCSRFToken()?>">

	<?php alert('auth.for_start_using')?>
	<?php t_field('full_name','user')?>

	<?php t_field('email','envelope')?>

	<?php t_field('password','lock','password',$password_attributes=['title'=>app('trans','auth.password_min_length',[],true)])?>
	<?php t_field('password_confirmation','lock','password',$password_attributes)?>

	<?php t_field('phone','phone','text',['title'=>app('trans','auth.phone_format',[],true)])?>
	<div class="row">

		<div class="col-sm-9">
			<?php t_field('avatar','image','file',['accept'=>'image/jpeg,image/png,image/gif'])?>

		</div>
		<div class="col-sm-3 text-center">
			<div class="thumbnail avatar" id="avatar_preview" >
				<img src="" >
			</div>
		</div>
	</div>

	<?php alert('auth.star_fields')?>


	<div class="text-center">
		<button class="btn btn-success" type="submit"><i class="fa fa-user-plus"></i> <?php app('trans','main.titles.signup')?></button>
	</div>
</form>
<?php };
$view->js_config=function ()use ($view)
{?>
	app.addForm('<?php app('url','auth/signup')?>',<?php Controllers\AuthController::getRulesForJS('postSignup')?>)
<?php };
include 'layout.php';