var app=
{
	'forms':{},
	/**
	 * Возвращает занчение по массиву ключей
	 * @param  {array} array массив данных
	 * @param  {array} keys  массив ключей
	 * @return {mixed}       значение
	 */
	'getArrayValue':function (array,keys)
	{
		if(keys.length==0)
			return array;
		key=keys.shift();
		return this.getArrayValue(array[key],keys);
	},
	/**
	 * Генерация локализованной строки
	 * @param  {string} name   условный адрес строки
	 * @param  {array} params массив данных замены
	 * @return {string}        локализованная строка
	 */
	'trans':function (name,params)
	{
		var str=this.getArrayValue(locale,name.split('.'));
		if(typeof str !='undefined'
			&& typeof params!='undefined')
			for(var key in params)
			{
				var val=params[key];
				str=str.replace(':'+key,val);
			}
		return str;
	},
	/**
	 * Вывод сообщения в лог браузера
	 * @param  {mixed} mess данные
	 * @param  {string} desc описание
	 * @param  {string} type тип сообщения
	 * @return {void}
	 */
	'log':function(mess, desc,type)
	{
		if(typeof type == 'undefined')
			type='log';
		if (typeof console != 'undefined' && typeof console.log == 'function')
		{
			if (!desc)
				desc='';
			else
				desc=desc.substr(0,13);
			desc+=':';
			if(typeof mess =='object')
				mess=$.extend({},mess);
			console[type](desc,mess);
		}
	},
	/**
	 * Добавить форму для обработки
	 * @param  {string} url   URL формы
	 * @param  {array} rules правила проверки
	 * @return {void}
	 */
	'addForm':function (url,rules)
	{
		var form={
			'obj':$('form[action="'+url+'"]'),
			'url':url,
			'rules':rules,
		}

		var has_required=false;
		for(var f_name in rules)
		{
			var rule=rules[f_name];
			var test_rule=rule;
			if(!(test_rule instanceof Array))
				test_rule=Object.values(test_rule);

			if(test_rule.indexOf('required')!=-1)
			{
				form.obj.find('label[for='+f_name+']').append('<span> *</span>');
				has_required=true;
			}
		}
		form.obj.submit(function (e)
		{
			e.preventDefault();
			var valid_data=app.checkForm(form.url);
			if(valid_data)
			{
				var form_data = new FormData();
				form_data.append('is_api_request',1);
				for(var f_name in valid_data)
				{
					var field_data=valid_data[f_name];
					form_data.append(f_name, field_data.value);
				}

				$.ajax(
				{
				  'url': form.url,
				  'data': form_data,
				  'processData': false,
				  'contentType': false,
				  'type': 'POST',
				  'dataType':'json',
				  'complete': function(response)
				  {
				  		json_response=response.responseJSON;

						if(typeof json_response.validation_errors !='undefined')
						{
							app.showErrors(valid_data,json_response.validation_errors)
						}
						if(typeof json_response.message!='undefined')
						{
							$('#modal_message').find('.modal-body').html(json_response.message).end().modal('show');
						}
						if(typeof json_response.redirect!='undefined')
						{
							if(json_response.redirect=='reload')
								setTimeout(function()
								{
									location.reload();
								}, (typeof json_response.redirect_timeout!='undefined'
										?json_response.redirect_timeout
										:0));

							else
								location.href=json_response.redirect;
							return;
						}

					},
				});
			}
			return false;
		});
		this.forms[form.url]=form;
	},
	/**
	 * Показ ошибок возле не правильных полей
	 * @param  {object} data   данные
	 * @param  {object} errors ошибки
	 * @return {void}
	 */
	'showErrors':function (data ,errors)
	{
		for(var f_name in data)
		{
			var field_data=data[f_name];
			var el=field_data.obj.parents('.form-group:first');
			var err_block=el.data('err_block');
			if(err_block)
				err_block.remove();
			if(typeof errors[f_name]!='undefined')
			{
				var err_block=$('<div class="alert alert-danger"><i class="fa fa-warn"></i>'+errors[f_name]+'</div>');
				el.after(err_block);
				el.data('err_block',err_block);
			}
		}
	},
	/**
	 * Проверка формы
	 * @param  {string} url URL формы
	 * @return {mixed}     false если данные не прошили проверку и данные сли прошил
	 */
	'checkForm':function (url)
	{
		var form=this.forms[url];
		var is_valid=true;
		var data={};
		var errors={};
		for(var f_name in form.rules)
		{
			var field=form.obj.find('[name='+f_name+']');
			var value=field.val();
			if(value && field.attr('type').toLowerCase()=='file')
			{
				value=field[0].files[0];
			}
			data[f_name]=
			{
				'rules':form.rules[f_name],
				'obj':field,
				'value':value,
			}
		}
		for(var f_name in data)
		{
			var field_data=data[f_name];
			for(var key in field_data.rules)
			{

				var rule_arr=field_data.rules[key].split(':');
				var rule_name=rule_arr.shift();
				var params=rule_arr;

				if(typeof validation_rules[rule_name]=='undefined')
					app.log('Validation rule '+rule_name+' doesn`t exists','','error');
				else
				{
					var validation =new validation_rules[rule_name](f_name,params,data,rule_name);
					if(validation.needCheck() && !validation.isValid())
					{
						errors[f_name]=(validation.getError());
						is_valid=false;
						break;
					}
				}
			}
		}
		this.showErrors(data,errors);
		return is_valid
			?data
			:false;
	},
};